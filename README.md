# Git cheatsheet

## Global Settings
- Related Setup: https://gist.github.com/hofmannsven/6814278
- Related Pro Tips: https://ochronus.com/git-tips-from-the-trenches/
- Interactive Beginners Tutorial: http://try.github.io/
- Git Cheatsheet by GitHub: https://services.github.com/on-demand/downloads/github-git-cheat-sheet/

## Ancestry reference - Relative Commit References
You may know that you can reference commits by their SHA, by tags, branches, and the special HEAD pointer. Sometimes that's not enough, though. There will be times when you'll want to reference a commit relative to another commit. For example, there will be times where you'll want to tell Git about the commit that's one before the current commit...or two before the current commit. There are special characters called "Ancestry References" that we can use to tell Git about these relative references. Those characters are:
^ – indicates the other parents (^2, for example, for a merge).
~ – indicates the first parent commit

- Here's how we can refer to previous commits:
the parent commit – the following indicate the parent commit of the current commit
  - HEAD^
  - HEAD~
  - HEAD~1
- the grandparent commit – the following indicate the grandparent commit of the current commit
  - HEAD^^
  - HEAD~2
- the great-grandparent commit – the following indicate the great-grandparent commit of the current commit
  - HEAD^^^
  - HEAD~3

The main difference between the ^ and the ~ is when a commit is created from a merge. A merge commit has two parents. With a merge commit, the ^ reference is used to indicate the first parent of the commit while ^2 indicates the second parent. The first parent is the branch you were on when you ran git merge while the second parent is the branch that was merged in. (Sai:  first parent 等於The tree most left commit is the first parent.)

## Merge Conflict Indicators Explanation
<img src="imgs/merge-conflict.gif" alt="conflict-gif" width="500"/>

source: Udacity - Version control with Git

The editor has the following merge conflict indicators:

`<<<<<<< HEAD`: everything below this line (until the next indicator) shows you what's on the current branch

`||||||| merged`: common ancestors everything below this line (until the next indicator) shows you what the original lines were

`=======`: is the end of the original lines, everything that follows (until the next indicator) is what's on the branch that's being merged in

`>>>>>>> heading-update` is the ending indicator of what's on the branch that's being merged in (in this case, the heading-update branch)

## Setup
See where Git is located:
`which git`

Get the version of Git:
`git --version`

Return current Git status of working directory :
`git status`

Create an alias (shortcut) for `git status`:
`git config --global alias.st status`


## General
Initialize Git:
`git init`

Get everything ready to commit:
`git add .`

Get custom file ready to commit:
`git add index.html`

Commit changes:
`git commit -m "Message"`

Commit changes with title and description:
`git commit -m "Title" -m "Description..."`

Add and commit in one step:
`git commit -am "Message"`

Update all changes:
`git add -u`

Remove files from Git:
`git rm index.html`

Remove file but do not track anymore:
`git rm --cached index.html`

Move or rename files:
`git mv index.html dir/index_new.html`

Undo modifications (restore files from latest commited version):
The -- is a way to tell Git to treat what follows checkout as a file and not as a branch.
`git checkout -- <file-name>`

Restore file from a custom commit (in current branch):
`git checkout 6eb715d -- index.html`

## Gitignore & Gitkeep
About: https://help.github.com/articles/ignoring-files

Useful templates: https://github.com/github/gitignore

Add or edit gitignore:
`nano .gitignore`

Track empty dir:
`touch dir/.gitkeep`

## Log
Show commits:
`git log`

Show changes:
`git log -p`

**[helpful]** Show  the actual changes made to files and things that have been changed in the commit:
`git log -p --stat`

Show stats and summary of commits:
`git log --stat --summary`

Show history of commits as graph:
`git log --graph`

Show oneline-summary of commits:
`git log --oneline`

**[helpful]** Show history of commits as graph-summary:
`git log --oneline --graph --all --decorate`

Show oneline-summary of the last three commits:
`git log --oneline -3`

Show only custom commits:
`git log --author="Sven"`
`git log --grep="Message"`
`git log --until=2013-01-01`
`git log --since=2013-01-01`

Show only custom data of commit:
`git log --format=short`
`git log --format=full`
`git log --format=fuller`
`git log --format=email`
`git log --format=raw`

Show every commit since special commit for custom file only:
`git log 6eb715d.. index.html`

Show changes of every commit since special commit for custom file only:
`git log -p 6eb715d.. index.html`

 Show commits each contributor has added to the repository:
`git shortlog`

## Update & Delete
Test-Delete untracked files:
`git clean -n`

Delete untracked files (not staging):
`git clean -f`

Unstage (undo adds):
`git reset HEAD index.html`

**[helpful]** Update most recent commit (also update the commit message):
`git commit --amend -m "New Message"`

## Compare
Compare modified files:
`git diff`

Compare modified files and highlight changes only:
`git diff --color-words index.html`

Compare modified files within the staging area:
`git diff --staged`

Compare branches:
`git diff master..branchname`

Compare branches like above:
`git diff --color-words master..branchname^`

Compare commits:
`git diff 6eb715d`
`git diff 6eb715d..HEAD`
`git diff 6eb715d..537a09f`

Compare commits of file:
`git diff 6eb715d index.html`
`git diff 6eb715d..537a09f index.html`

Compare without caring about spaces:
`git diff -b 6eb715d..HEAD` or:
`git diff --ignore-space-change 6eb715d..HEAD`

Compare without caring about all spaces:
`git diff -w 6eb715d..HEAD` or:
`git diff --ignore-all-space 6eb715d..HEAD`

Useful comparings:
`git diff --stat --summary 6eb715d..HEAD`


## Releases & Version Tags
Tagging is generally used to capture a point in history that is used for a marked version release (i.e. v1.0.1). A tag is like a branch that doesn’t change. Unlike branches, tags, after being created, have no further history of commits.

Show all released versions:
`git tag`

Show all released versions with comments:
`git tag -l -n1`

Create release version:
`git tag v1.0.0`

**[helpful]** Create release version with comment:
`git tag -a v1.0.0 -m 'Message'`

Checkout a specific release version:
`git checkout v1.0.0`

## Branch
Git branches are effectively a pointer to a snapshot of your changes. When you want to add a new feature or fix a bug—no matter how big or how small—you spawn a new branch to encapsulate your changes. This makes it harder for unstable code to get merged into the main code base, and it gives you the chance to clean up your future's history before merging it into the main branch.

Show branches:
`git branch`

Create branch:
`git branch branchname`

Create branch on specific commit:
`git branch branchname a_commit_SHA`

Change to branch:
`git checkout branchname`

Create and change to new branch:
`git checkout -b branchname`

Rename branch:
`git branch -m branchname new_branchname` or:
`git branch --move branchname new_branchname`

Show all completely merged branches with current branch:
`git branch --merged`

Delete merged branch (only possible if not HEAD):
`git branch -d branchname` or:
`git branch --delete branchname`

Delete not merged branch:
`git branch -D branch_to_delete`

## Merge
**Create a backup branch before using `git reset`**

If you created the backup branch prior to resetting anything, then you can easily get back to having the master branch point to the same commit as the backup branch.
You'll just need to:
- remove the uncommitted changes from the working directory
- merge backup into master (which will cause a Fast-forward merge and move master up to the same point as backup)

`git checkout -- index.html`

`git merge backup`

---
`git merge branchname`

Merge to master (only if fast forward):
`git merge --ff-only branchname`

Merge to master (force a new commit):
`git merge --no-ff branchname`

Stop merge (in case of conflicts):
`git merge --abort`

Undo local merge that hasn't been pushed yet:
`git reset --hard origin/master`

Merge only one specific commit:
`git cherry-pick 073791e7`


## Revert & Reset
- Reverting creates a new commit that reverts or undos a previous commit.
- Resetting, on the other hand, erases commits!
  - --mixed
  - --soft  
  - --hard

  Remember that using the git reset command will erase commits from the current branch. So if you want to follow along with all the resetting stuff that's coming up, you'll need to create a branch on the current commit that you can use as a backup.

  Before I do any resetting, I usually create a backup branch on the most-recent commit so that I can get back to the commits if I make a mistake.


Go back to commit:
`git revert 073791e7dd71b90daa853b2c5acc2c925f02dbc6`

Soft reset (Move the changes that were made in the commit  to the staging index):
`git reset --soft 073791e7dd71b90daa853b2c5acc2c925f02dbc6`

**[helpful]** Undo latest commit: `git reset --soft HEAD~ `

Mixed reset (Default reset, the changes that were made in the commit are applied to the files in the working directory.):
`git reset --mixed 073791e7dd71b90daa853b2c5acc2c925f02dbc6`

Hard reset (Throw out all of the changes that were made in the commit):
`git reset --hard 073791e7dd71b90daa853b2c5acc2c925f02dbc6`

Hard reset of a single file (`@` is short for `HEAD`):
`git checkout @ -- index.html`

## Collaborate
**Tracking branch**: The branch that appears in the local repository tracking a branch in the remote repository. (normally it's called origin/master branch, but you can change the name)

**Clone VS Fork:**  Forking is an action that's done on a hosting service, like GitHub. Forking a repository creates *an identical copy* of the original repository and moves this copy to your account. You have total control over this forked repository. Modifying your forked repository does not alter the original repository in any way.

**Origin vs Upstream Clarification:**
- When Clone: Git created a connection for us automatically and gave this connection a short name of **origin** when we clone a project. We can manually set up a connection to the original or source remote repository, though. Typically, we'd name the short name **upstream**.
- When Fork: What might be confusing is that origin does not refer to the source repository (also known as the "original" repository) that we forked from. Instead, it's pointing to our forked repository. So even though it has the word **origin** is not actually the original repository.
- You can actually change the name of **origin** to mine and the name of **upstream** to source-repo.

---

Show remote details:
`git remote -v`

Clone to localhost:
`git clone https://github.com/user/project.git` or:
`git clone ssh://user@domain.com/~/dir/.git`

Clone to localhost folder:
`git clone https://github.com/user/project.git ~/dir/folder`

Clone specific branch to localhost:
`git clone -b branchname https://github.com/user/project.git`

Clone with token authentication (in CI environment):
`git clone https://oauth2:<token>@gitlab.com/username/repo.git`

---
**Fetch VS Pull:**  `git pull` is the same thing as a `git fetch` + `git merge`!
One main point when you want to use git fetch rather than git pull is if your remote branch and your local branch both have changes that neither of the other ones has.

In this case, you want to fetch the remote changes to get them in your local branch and then perform a merge manually. Then you can push that new merge commit back to the remote.


Push (set default with `-u`):
`git push -u origin master`

Push:
`git push origin master`

Force-Push:
`git push origin master --force`

Delete remote branch (push nothing):
`git push origin :branchname` or:
`git push origin --delete branchname`

Pull:
`git pull`

Pull specific branch:
`git pull origin branchname`

Fetch a pull request on GitHub by its ID and create a new branch:
`git fetch upstream pull/ID/head:new-pr-branch`

---

Add remote upstream from GitHub project:
`git remote add upstream https://github.com/user/project.git`

Add remote upstream from existing empty project on server:
`git remote add upstream ssh://root@123.123.123.123/path/to/repository/.git`

Fetch:
`git fetch upstream`

Fetch a custom branch:
`git fetch upstream branchname:local_branchname`

Merge fetched commits:
`git merge upstream/master`

Remove origin:
`git remote rm origin`

Show remote branches:
`git branch -r`

Show all branches (remote and local):
`git branch -a`

Create and checkout branch from a remote branch:
`git checkout -b local_branchname upstream/remote_branchname`

Compare:
`git diff origin/master..master`


## Squash Commits
**Important:** Whenever you rebase commits, Git will create a new SHA for *each commit*! This has drastic implications!
So you should not rebase if you have already pushed the commits you want to rebase. If you're collaborating with other developers, then they might already be working with the commits you've pushed.
They will have to do some complicated surgery to their Git repository to get their repo back in a working state...and it might not even be possible...

**Tips:**
- After using `git rebase` you have to force push the branch, because GitHub was trying to prevent me from accidentally erasing the a few separate commits.  

- A good practice is, before you do `git rebase`, you create a backup branch for those separated commits.  
---

Rebase:
`git checkout branchname` » `git rebase master`
or:
`git merge master branchname`
(The rebase moves all of the commits in `master` onto the tip of `branchname`.)

Cancel rebase:
`git rebase --abort`

Squash multiple commits into one:
`git rebase -i HEAD~3` ([source](https://www.devroom.io/2011/07/05/git-squash-your-latests-commits-into-one/))

Squash-merge a feature branch (as one commit):
`git merge --squash branchname` (commit afterwards)

## Stash
Stash the changes in a dirty working directory away

Put in stash:
`git stash save "Message"`

Show stash:
`git stash list`

Show stash stats:
`git stash show stash@{0}`

Show stash changes:
`git stash show -p stash@{0}`

Use custom stash item and drop it:
`git stash pop stash@{0}`

Use custom stash item and do not drop it:
`git stash apply stash@{0}`

Use custom stash item and index:
`git stash apply --index`

Create branch from stash:
`git stash branch new_branch`

Delete custom stash item:
`git stash drop stash@{0}`

Delete complete stash:
`git stash clear`


## Archive
Create a zip-archive: `git archive --format zip --output filename.zip master`

Export/write custom log to a file: `git log --author=sven --all > log.txt`

## Troubleshooting
Ignore files that have already been committed to a Git repository: http://stackoverflow.com/a/1139797/1815847

## Security
Hide Git on the web via `.htaccess`: `RedirectMatch 404 /\.git`
(more info here: http://stackoverflow.com/a/17916515/1815847)

## Large File Storage
Website: https://git-lfs.github.com/

Install: `brew install git-lfs`

Track `*.psd` files: `git lfs track "*.psd"` (init, add, commit and push as written above)
